# v6-server-core
REST сервисы для игр

## запуск сервера

с настрйоками по умолчанию
```
$ node index.js
```

с пользовательскими настройками
```
$ node index.js --conf="./conf.js"
```

настройки игры
```js
    test2: {
        modes: {
            mode_1: {},
            mode_2: {}
        },
        initData: {
            turnTime: 20000,
            timeMode: 'reset_every_switch',
            timeStartMode: 'after_round_start',
            addTime: 0,
            takeBacks: 0,
            maxTimeouts: 1,
            minTurns: 0
        },
        engine: Engine
        clientOpts: {
            modes: ['mode_1', 'mode_2']
        }
    }
```
