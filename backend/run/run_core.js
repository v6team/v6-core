'use strict';

let gameConf = require('../conf.js'),
    GameServer = require('../server.js'),
    co = require('co'),
    gameServer = new GameServer(gameConf);

co(function* () {
    yield gameServer.start();
});