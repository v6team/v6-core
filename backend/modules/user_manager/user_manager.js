'use strict';

//require
let Manager = require('../../lib/manager.js');
let User = require('./user.js');
let co = require('co');

let moduleName = 'UserManager';
let logger, log, err, wrn;

const SENDER_USER = 'user';
const SENDER_SERVER = 'server';

let defaultConf = {
    checkAuth: function (userId, userName, game, sign) {
        return (userId && userName && sign);
    }
};

module.exports = class UserManager extends Manager {
    constructor(server, conf) {
        logger = server.logger.getLogger(moduleName);
        log = logger.log;
        wrn = logger.wrn;
        err = logger.err;

        conf = Object.assign(defaultConf, conf);
        super(server, conf);

        this.eventBus = server.eventBus;
        this.games = this.server.conf.games;

        log(`constructor`, `userManager created, conf: ${JSON.stringify(conf)}`);
    }

    *init() {
        try {
            yield this.test();
            yield this.loadUsersRanks();
            this.initEvents();
            this.isRunning = true;
            log(`init`, `init success`);
        } catch (e) {
            this.isRunning = false;
            err(`init`, `error: ${e.stack}`);
            throw Error(`init failed`);
        }
    }

    initEvents() {
        this.eventBus.on(`system.socket_disconnect`, (message) => {
            return this.onNewMessage(message);
        });
        this.eventBus.on(`system.user_disconnect`, (message) => {

        });
        this.eventBus.on(`system.user_leave_game`, (message) => {

        });
        this.eventBus.on(`system.update_user_rank`, (game, mode, user) => {
            return this.updateUserRank(game, mode, user);
        });
        this.eventBus.on(`user_manager.*`, (message) => {
            return this.onNewMessage(message);
        });
    }

    test() {
        log(`test`, `start test`);
        return super.test()
            .then(res => {
                return res;
            });
    }

    onNewMessage(message) {
        let self = this;
        return co(function* () {
            message = yield self.addToList(message);
            if (!message) {
                return true;
            }
            else {
                return self.onMessage(message);
            }
        });
    }

    addToList(message) {
        let self = this, game = message.game, userId = message.user.userId;
        return co(function* () {
            message = JSON.stringify(message);
            log(`addToList`, `obj: ${message}`);
            yield self.memory.listAdd(`user_messages_list:${game}:${userId}`, message);
            return self.getCurrentMessage(game, userId);
        });
    }

    getCurrentMessage(game, userId) {
        return this.memory.listGet(`user_messages_list:${game}:${userId}`, `user_messages_current:${game}:${userId}`)
            .then((message) => {
                if (message) {
                    message = JSON.parse(message);
                }
                return message;
            });
    }

    onMessage(message) {
        let self = this, game = message.game, userId = message.user.userId, data = message.data;
        log(`onMessage`, `game: ${JSON.stringify(message)}`);
        return co(function* () {
            let user;
            if (message.type !== 'login') {
                user = yield self.memory.getUser(game, message.user.userId);
                if (!user) {
                    yield self.memory.del(`user_messages_current:${game}:${userId}`);
                    yield self.memory.del(`user_messages_list:${game}:${userId}`);
                    wrn(`onMessage`, ` no user data, userId: ${message.user.userId}, game: ${message.game}`);
                    return false;
                }
            }

            switch (message.type) {
                case 'login':
                    yield self.onUserLogin(message.user, data);
                    break;
                case 'settings': // player ready to play
                    yield self.onUserChanged(user, game, data);
                    yield self.eventBus.emit(`system.save_settings`, game, user.userId, data);
                    break;
                case 'changed':
                    yield self.onUserChanged(user, game, data);
                    break;
                case 'disconnect':
                    yield self.onUserDisconnect(user, game, data);
                    break;
                case 'leave':
                    yield self.onUserLeaveGame(user, game, data);
            }

            // get next user message
            yield self.memory.del(`user_messages_current:${game}:${userId}`);
            message = yield self.getCurrentMessage(game, userId);
            if (!message) {
                return true;
            }
            else {
                return self.onMessage(message);
            }
        });
    }

    onUserLogin(socket, loginData) {
        let self = this, userId = loginData.userId, userName = loginData.userName, game = loginData.game;

        log(`onUserLogin`, `new login, socketId: ${socket.socketId}, userId: ${userId}, game: ${game}, sign: ${loginData.sign}`);
        if (!socket.socketId || !socket.serverId || !loginData.userId || loginData.userId === "undefined") {
            err(`onUserLogin`, `wrong socket ${socket}`);
            return Promise.resolve(false);
        }

        if (!this.checkLoginData(loginData)) {
            return Promise.resolve(false);
        }

        if (!this.games[loginData.game]) {
            err(`onUserLogin`, `wrong game to login, ${loginData.game}`);
            return Promise.resolve(false);
        }
        return co(function* () {
            let user = yield self.memory.getUser(game, userId);
            if (user) {
                return self.onUserRelogin(socket, user, loginData);
            } else {
                yield self.memory.setSocketData(socket.socketId, socket.serverId, loginData.userId, loginData.userName, loginData.game);
                let data = yield self.loadUserData(loginData.userId, loginData.userName, loginData.game);

                user = new User(data.userData, game, self.games[game].modes);

                yield self.loadUserRanks(game, user);
                yield self.memory.addUser(game, user);
                // enter room
                yield self.memory.setUserSocket(game, userId, socket.socketId, socket.serverId);

                yield self.sendUserLoginData(socket, game, data);

                yield self.eventBus.emit(`system.send_to_sockets`, game, `{ "module":"server", "type": "user_login", "data": ${user.getDataToSend(true)} }`);

                log(`onUserLogin`, `userData: ${JSON.stringify(data)}`);
            }
        });
    }

    onUserRelogin(socket, user, loginData) {
        let self = this, userId = loginData.userId, userName = loginData.userName, game = loginData.game;
        log(`onUserRelogin`, `userId: ${userId} `);
        return co(function* () {
            // update user data
            let oldSocket = yield self.memory.getUserSocket(game, userId);
            log(`onUserRelogin`, `oldSocket: ${JSON.stringify(oldSocket)}`);
            if (oldSocket) {
                yield self.memory.removeSocketData(oldSocket.socketId);
                // close old socket
                yield self.eventBus.emit(`system.send_to_socket`, oldSocket, {
                    module: 'server',
                    type: 'error',
                    data: 'new_connection'
                });
                // TODO: close old socket from oldSocket.server;
            }
            yield self.memory.setSocketData(socket.socketId, socket.serverId, user.userId, user.userName, user.game);
            yield self.memory.setUserSocket(game, userId, socket.socketId, socket.serverId);
            let data = yield self.loadUserData(loginData.userId, loginData.userName, loginData.game);
            yield self.sendUserLoginData(socket, game, data);

            yield self.eventBus.emit(`system.user_relogin`, game, user.getDataToSend());
        });
    }

    onUserDisconnect(user, game, socket) {
        let self = this;
        log(`onUserDisconnect`, `game: ${game}, user: ${user.userId}, socketId: ${socket.socketId}`);
        return co(function* () {
            // get user socket and check
            yield self.memory.removeUserSocket(game, user.userId);

            let userRoom = yield self.memory.getUserRoom(user.userId, game);

            if (userRoom) {
                yield self.eventBus.emit(`system.user_disconnect`, user, userRoom);
            } else {
                // remove from userlist
                yield self.memory.removeUser(game, user.userId);
                // leave game room
                yield self.eventBus.emit(`system.send_to_sockets`, game, `{"module":"server", "type": "user_leave", "data": "${user.userId}"}`);
            }
        });
    }

    onUserLeaveGame(user, game) {
        let self = this, userId = user.userId;
        log(`onUserLeaveGame`, `game: ${game}, userId: ${userId}`);

        return co(function* () {
            let socketData = yield self.memory.getUserSocket(game, userId);

            if (socketData) {
                return false;
            }

            // remove from userlist
            yield self.memory.removeUser(game, user.userId);
            // leave game room
            yield self.eventBus.emit(`system.send_to_sockets`, game, `{"module":"server", "type": "user_leave", "data": "${user.userId}"}`);
        });
    }

    onUserChanged(user, game, data) {
        data = data || {};
        log(`onUserChanged`, `user  ${user.userId}, ${game}, changed, ${JSON.stringify(data)}`, 1);
        let self = this, userId = user.userId;
        return co(function* () {
            // update user data active
            if (typeof data.isActive === "boolean") {
                yield self.memory.hashAdd(`user_data:${game}:${userId}`, 'isActive', data.isActive.toString());
            }
            if (typeof data.disableInvite === "boolean") {
                yield self.memory.hashAdd(`user_data:${game}:${userId}`, 'disableInvite', data.disableInvite.toString());
            }
            user = yield self.memory.getUser(game, userId);
            if (!user) {
                wrn(`onUserChanged`, `user ${userId}, not exists`);
                return false;
            }
            yield self.sendUserInfo(game, user);
        });
    }

    sendUserLoginData(socket, game, data) {
        let self = this, clientOpts = this.games[game].clientOpts;
        return co(function* () {
            let userlist = yield self.memory.getUserList(game),
                waiting = yield self.memory.getWaitingUsers(game),
                rooms = yield self.memory.getRooms(game),
                userData = data.userData;

            let message = JSON.stringify({
                "module": "server",
                "type": "login",
                "data": {
                    "you": userData,
                    "userlist": userlist,
                    "rooms": rooms,
                    "waiting": waiting,
                    "settings": data.settings,
                    "opts": clientOpts,
                    "ban": data.ban
                }
            });
            yield self.eventBus.emit(`system.send_to_socket`, socket, message);
        });
    }

    sendUserInfo(game, user) {
        return this.eventBus.emit(`system.send_to_sockets`, game, {
            module: "server",
            type: "user_changed",
            data: user.getDataToSend()
        });
    }

    checkLoginData(loginData) {
        if (!loginData.userId || !loginData.userName || !loginData.sign || !loginData.game) {
            wrn(`checkLoginData`, `wrong loginData: ${loginData.userId}, ${loginData.userName}, ${loginData.sign}, ${loginData.game}`);
            return false;
        } else {
            return this.conf.checkAuth(loginData.userId, loginData.userName, loginData.game, loginData.sign);
        }
    }

    loadUserData(userId, userName, game) {
        let defaultData = this.games[game];
        return this.eventBus.trigger(`system.load_user_data`, game, userId).then((loadedUserData) => {
            log(`loadUserData`, `userId: ${userId}, data: ${JSON.stringify(loadedUserData)}`);
                loadedUserData = loadedUserData || {};
                loadedUserData.userData = loadedUserData.userData || {};
                loadedUserData.settings = loadedUserData.settings || {};

                let userData = {
                    userId: userId, userName: userName
                };

                userData.dateCreate = loadedUserData.userData.dateCreate || Date.now();
                userData.isActive = true;
                userData.disableInvite = loadedUserData.settings.disableInvite || false;
                userData.isBanned = !!loadedUserData.ban || false;


                let modes = defaultData.modes;
                for (let mode of Object.keys(modes)) {
                    if (!loadedUserData.userData[mode]) {
                        // no saved user data in mode
                        userData[mode] = modes[mode];
                    } else {
                        userData[mode] = {};
                        for (let prop of Object.keys(modes[mode])) {
                            userData[mode][prop] = loadedUserData.userData[mode][prop] || modes[mode][prop];
                        }
                    }
                }
                return {
                    userData: userData,
                    ban: loadedUserData.ban || false,
                    settings: loadedUserData.settings || {}
                };
            });
    }

    *loadUsersRanks() {
        for (let gameKey of Object.keys(this.games)) {
            let game = this.games[gameKey];
            for (let mode of Object.keys(game.modes)) {
                let usersRanks = yield this.eventBus.trigger(`system.load_user_ranks`, gameKey, mode);
                if (usersRanks && usersRanks.length){
                    log(`loadUsersRanks`, `${gameKey}.${mode} userRanks: ${usersRanks.length}`);
                    for (let user of usersRanks) {
                        yield this.memory.addUserRank(gameKey, mode, user.userId, user[mode].ratingElo);
                    }
                }
            }
        }
    }

    *loadUserRanks(game, user) {
        let gameData = this.games[game];
        let modes = gameData.modes;
        for (let modeKey of Object.keys(modes)) {
            let defaultModeData = modes[modeKey];
            if (defaultModeData.ratingElo && user.getMode(modeKey).ratingElo > defaultModeData.ratingElo){
                yield this.memory.addUserRank(game, modeKey, user.userId, user.getMode(modeKey).ratingElo);
            }
            let rank = yield this.memory.getUserRank(game, modeKey, user.userId);
            user.getMode(modeKey).rank = rank;
            log(`loadUserRanks`, `userId: ${user.userId}, rank: ${rank}, elo: ${user.getMode(modeKey).ratingElo}`);
        }

    }

    updateUserRank(game, mode, user) {
        let userId = user.userId, score = user.getMode(mode).ratingElo;
        return this.memory.addUserRank(game, mode, userId, score);
    }
};