module.exports = {
    logger: {
        priority: 3
        //,showOnly: []
    },
    taskQueue: {
        redis: {
            host: '127.0.0.1',
            port: '6379'
        }
    },
    memory: {
        clear: true,
        redis: {
            host: '127.0.0.1',
            port: '6379'
        }
    },
    managers: [
        {
            name: 'socketManager',
            conf: {
                port: '8078'
            }
        },
        {
            name: 'gameManager',
            conf: {}
        },
        {
            name: 'inviteManager',
            conf: {}
        },
        {
            name: 'userManager',
            conf: {}
        },
        {
            name: 'chatManager',
            conf: {}
        }
    ],

    gamesConf: {
        games: {
            test1: {
                modes: {
                    default: {
                        ratingElo: 1600
                    }
                },
                clientOpts: {
                    modes: ['default']
                }
            },
            test2: {
                modes: {
                    mode_1: {},
                    mode_2: {}
                },
                initData: {
                    saveHistory: true,
                    saveRating: true,
                    turnTime: 20000,
                    timeMode: 'reset_every_switch',
                    timeStartMode: 'after_round_start',
                    addTime: 0,
                    takeBacks: 0,
                    maxTimeouts: 1,
                    minTurns: 0
                },
                clientOpts: {
                    modes: ['mode_1', 'mode_2']
                }
            }
        },
        modes: {
            default: {}
        },
        modeData: {
            win: 0,
            lose: 0,
            draw: 0,
            games: 0,
            rank: 0,
            ratingElo: 1600,
            timeLastGame: 0
        },
        initData: {
            saveHistory: true,
            saveRating: true,
            turnTime: 20000,
            timeMode: 'reset_every_switch',
            timeStartMode: 'after_round_start',
            addTime: 0,
            takeBacks: 0,
            maxTimeouts: 1,
            minTurns: 0
        },
        clientOpts: {
            modes: ['default']
        },
        mode: 'develop'
    }
};