'use strict';

let argv = require('minimist')(process.argv.slice(2)),
    fs = require('fs'),
    GameServer = require('./server.js'),
    apiConf,
    server,
    confPath = argv['conf'];

if (confPath){
    fs.exists(confPath, (exists) => {
        if (exists) {
            console.log(`start with conf file ${confPath}`);
            apiConf = require(confPath);
            server = new GameServer(apiConf);
            server.start();
        } else {
            console.log(`conf file ${confPath} not exists`);
        }
    });
} else {
    apiConf = require('./conf.js');
    server = new GameServer(apiConf);
    server.start();
}
